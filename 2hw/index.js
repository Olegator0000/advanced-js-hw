// функцію try..catch ми використовуємо коли нам треба перевірити на наявність помилки в певній функції,
//     і якщо буде помилка то функція не спрацює і видасть помилку.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function createList(books){
    const root =document.getElementById("root");
    const ul = document.createElement("ul")

books.forEach(book =>{
    try {
        if(!book.author || !book.name || !book.price){
            throw new Error('обʼєкт не містить всі властивості')
        }
const li = document.createElement('li')
        li.textContent = `${book.author}: ${book.name}, ціна: ${book.price}`
        ul.appendChild(li);
    }catch (err){
        console.error(err)
    }

})
    root.appendChild(ul)
}
createList(books)
