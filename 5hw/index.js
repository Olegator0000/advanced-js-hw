class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
        this.render();
    }

    render() {
        const cardContainer = document.createElement('div');
        cardContainer.classList.add('card');

        const title = document.createElement('h2');
        title.textContent = this.post.title;

        const body = document.createElement('p');
        body.textContent = this.post.body;

        const author = document.createElement('p');
        author.textContent = `Author: ${this.user.name}`;

        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'Delete';
        deleteButton.addEventListener('click', () => {
            this.delete();
        });

        cardContainer.appendChild(title);
        cardContainer.appendChild(body);
        cardContainer.appendChild(author);
        cardContainer.appendChild(deleteButton);

        document.body.appendChild(cardContainer);
    }

    delete() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: 'DELETE'
        })
            .then(response => {
                if (response.ok) {
                    document.querySelector('.card').remove();
                }
            });
    }
}

class API {
    static async fetchData() {
        const usersResponse = await fetch('https://ajax.test-danit.com/api/json/users');
        const users = await usersResponse.json();

        const postsResponse = await fetch('https://ajax.test-danit.com/api/json/posts');
        const posts = await postsResponse.json();

        return { users, posts };
    }
}

(async () => {
    const { users, posts } = await API.fetchData();

    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);
        if (user) {
            new Card(post, user);
        }
    });
})();
