
// 1)Прототипне наслідування це спосыб за допомогою якого обʼєкти можуть наслідувати властивості та методи іншого обʼєкту

// 2)super() треба викликати коли ви спадкуєте і розширюєте інформацію батьківського класу
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(name) {
        this._name = name;
    }

    get age() {
        return this._age;
    }
    set age(age) {
        this._age = age;
    }

    get salary() {
        return this._salary;
    }
    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }
    set lang(lang) {
        this._lang = lang;
    }

    get ProgrammerSalary() {
        return this._salary * 3;
    }
}

let prog1 = new Programmer("Oleh", 18, 50000, ["JavaScript", "Java"]);
let prog2 = new Programmer('Andrew', 18, 35000, ['C#', 'python']);

console.log(prog1);
console.log(prog2);
